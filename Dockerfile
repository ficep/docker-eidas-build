# Copyright 2017 Paolo Smiraglia <paolo.smiraglia@gmail.com>
#                TORSEC Group (http://security.polito.it)
#                Politecnico di Torino
# 
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence").
# 
# You may not use this work except in compliance with the Licence.
# 
# You may obtain a copy of the Licence at:
# 
#    https://joinup.ec.europa.eu/software/page/eupl
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations
# under the Licence.

FROM maven:alpine
MAINTAINER "Paolo Smiraglia <paolo.smiraglia@gmail.com>"

# install dependencies
RUN apk add --no-cache --update \
        curl \
        git \
        gnupg \
        tar \
        unzip \
    && rm -fr /var/cache/apk/*

# install Java Cryptography Extensions 8
RUN curl -jkSsLH "Cookie: oraclelicense=accept-securebackup-cookie" \
        "http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip" \
        -o /tmp/jce.zip \
    && unzip -jo -d ${JAVA_HOME}/jre/lib/security /tmp/jce.zip \
    && rm -fr /tmp/jce.zip

# add script to clone eIDAS sources
ADD get-eidas-sources.sh /usr/local/bin/get-eidas-sources
RUN chmod +x /usr/local/bin/get-eidas-sources

# create and change to working dir
RUN mkdir /sources
WORKDIR /sources
